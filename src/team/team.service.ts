import { Injectable } from '@nestjs/common';
import { CreateTeamDto } from './dto/create-team.dto';
import { UpdateTeamDto } from './dto/update-team.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Team } from './entities/team.entity';
import { Repository } from 'typeorm';
import { PaginateQuery, paginate } from 'nestjs-paginate';

@Injectable()
export class TeamService {

  constructor(
    @InjectRepository(Team)
    private teamRepository: Repository<Team>,
  ){}

  async create(createTeamDto: CreateTeamDto) {
    const team = this.teamRepository.create(createTeamDto);
    await this.teamRepository.save(team);
    return team;
  }

  findAll(filtres: PaginateQuery) {
    return paginate(filtres, this.teamRepository, {
      sortableColumns: ['id', 'name'],
      defaultSortBy: [['id', 'DESC']],
      searchableColumns: ['name'],
      relations: ['sector', 'userMaster'],
      select: ['id', 'name', 'sector', 'userMaster'],
      filterableColumns: {},
    });
  }

  findOne(id: number) {
    return this.teamRepository.findOne({
      relations: ['sector', 'userMaster'],
      where: {
        id: id 
      }
    });
  }

  update(id: number, updateTeamDto: UpdateTeamDto) {
    return this.teamRepository.update(id, updateTeamDto);
  }

  remove(id: number) {
    return this.teamRepository.delete(id);
  }
}
