import {
    Column,
    Entity,
    JoinColumn,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
  } from 'typeorm';
  import { Contract } from '../../contract/entities/contract.entity';
  import { User } from '../../user/entities/user.entity';
  import { Sector } from '../../sector/entities/sector.entity';
  
  @Entity()
  export class Team {
    @PrimaryGeneratedColumn()
    id: number;
  
    @Column()
    name: string;
  
    @ManyToOne(() => Sector, (sector) => sector.teams)
    @JoinColumn({ name: 'sector_id' })
    sector: Sector;
  
    @OneToMany(() => User, (user) => user.team)
    users: User[];
  
    @ManyToOne(() => User, (user) => user.teamsMaster)
    @JoinColumn({ name: 'user_master_id' })
    userMaster: User;
  
    @OneToMany(() => Contract, (contract) => contract.teamTechnique)
    contractTechnique: Contract[];
  }
  