import { Injectable } from '@nestjs/common';
import { CreateCustomerDto } from './dto/create-customer.dto';
import { UpdateCustomerDto } from './dto/update-customer.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Customer } from './entities/customer.entity';
import { Repository } from 'typeorm';
import { PaginateQuery, paginate } from 'nestjs-paginate';

@Injectable()
export class CustomerService {

  constructor(
    @InjectRepository(Customer)
    private customerRepository: Repository<Customer>,
  ){}
  
  async create(createCustomerDto: CreateCustomerDto) {
    const customer = this.customerRepository.create(createCustomerDto);
    await this.customerRepository.save(customer);
    console.log('This action adds a new customer')
    return ;
  }

  findAll(filtres: PaginateQuery) {
    return paginate(filtres, this.customerRepository, {
      sortableColumns: ['id', 'firstName', 'lastName'],
      defaultSortBy: [['id', 'DESC']],
      searchableColumns: ['firstName', 'lastName'],
      select: [
        'id',
        'firstName',
        'lastName', 
        'city',
        'zipCode',
        'address',
        'status',
      ], 
      filterableColumns: {},
    });
    
  }

  findOne(id: number) {
    return this.customerRepository.findOneBy({id});
  }

  update(id: number, updateCustomerDto: UpdateCustomerDto) {
    return this.customerRepository.update(id, updateCustomerDto);
  }

  remove(id: number) {
    return this.customerRepository.delete(id);
  }
}
