import { ApiProperty } from "@nestjs/swagger";

export class CreateCustomerDto {
    @ApiProperty({ description: 'civility'})
    civility?: string;

    @ApiProperty({ description: 'firstName'})
    firstName: string;
    
    @ApiProperty({ description: 'lastname'})
    lastname: string;

    @ApiProperty({ description: 'city'})
    city: string;

    @ApiProperty({ description: 'zipCode'})
    zipCode: string;

    @ApiProperty({ description: 'address'})
    address: string;

    @ApiProperty({ description: 'status'})
    status: string;

    @ApiProperty({ description: 'mobile'})
    mobile: string; 

    @ApiProperty({ description: 'email'})
    email: string;
}
