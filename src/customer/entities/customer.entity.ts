import {
    Column,
    CreateDateColumn,
    Entity,
    OneToMany,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
  } from 'typeorm';
  import { Contract } from '../../contract/entities/contract.entity';
  
  @Entity()
  export class Customer {
    @PrimaryGeneratedColumn()
    id: number;
  
    @Column({ nullable: true })
    civility: string;
  
    @Column({ name: 'first_name', nullable: true })
    firstName: string;
  
    @Column({ name: 'social_reason', nullable: true })
    socialReason: string;
  
    @Column({ name: 'last_name', nullable: true })
    lastName: string;
  
    @Column({ nullable: true })
    address: string;
  
    @Column({ nullable: true })
    city: string;
  
    @Column({ nullable: true })
    status: string;
  
    @Column({ name: 'zip_code', nullable: true })
    zipCode: string;
  
    @Column({ name: 'birthday', nullable: true, type: 'datetime' })
    birthday: Date;
  
    @Column({ name: 'birthdayPlace', nullable: true })
    birthdayPlace: string;
  
    @Column({ nullable: true, type: 'text' })
    note: string;
  
    @Column({ name: 'country', nullable: true, type: 'text' })
    country: string;
  
    @Column({ name: 'email', nullable: true, type: 'text' })
    email: string;
  
    @Column({ name: 'mobile', nullable: true, type: 'text' })
    mobile: string;
  
    @Column({ name: 'phone', nullable: true, type: 'text' })
    phone: string;
  
    @CreateDateColumn({ name: 'created', type: 'datetime' })
    created: Date;
  
    @UpdateDateColumn({ name: 'updated', type: 'datetime' })
    updated: Date;
  
    @OneToMany(() => Contract, (contract) => contract.customer)
    contracts: Contract[];
  }
  