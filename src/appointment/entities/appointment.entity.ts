import {
    Column,
    Entity,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
  } from 'typeorm';
  import { Contract } from '../../contract/entities/contract.entity';
  import { User } from '../../user/entities/user.entity';
  
  @Entity()
  export class Appointment {
    @PrimaryGeneratedColumn()
    id: number;
  
    @Column({ name: 'maintenance_index' })
    maintenanceIndex: number;
  
    @Column({ name: 'start_date', type: 'datetime' })
    startDate: Date;
  
    @Column({ name: 'end_date', type: 'datetime' })
    endDate: Date;
  
    @ManyToOne(() => Contract, (contract) => contract.appointments)
    @JoinColumn({ name: 'contract_id' })
    contract: Contract;
  
    @Column({ name: 'is_scheduled', type: 'boolean', default: 0 })
    isScheduled: boolean;
  
    @Column({ name: 'is_valid', type: 'boolean', default: 0 })
    isValid: boolean;
  
    @Column({ name: 'client_has_been_contacted', default: 2 })
    clientHasBeenContacted: number;
  
    @Column({ name: 'is_archived', type: 'boolean', default: 0 })
    isArchived: boolean;
  
    @Column({ name: 'count_call', nullable: true })
    countCall: number;
  
    @ManyToOne(() => User, (user) => user.technicianAppointment)
    @JoinColumn({ name: 'technician_id' })
    technician: User;
  }
  