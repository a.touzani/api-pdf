import { Injectable } from '@nestjs/common';
import { CreateContractDto } from './dto/create-contract.dto';
import { UpdateContractDto } from './dto/update-contract.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Contract } from './entities/contract.entity';
import { Repository } from 'typeorm';
import { PaginateQuery } from 'nestjs-paginate';

@Injectable()
export class ContractService {

  constructor(
    @InjectRepository(Contract)
    private contractRepository: Repository<Contract>,
  ){}

  async create(createContractDto: CreateContractDto) {
    const contract = this.contractRepository.create(createContractDto);
    await this.contractRepository.save(contract);
    return contract;
  }

  findAll(/*filtres: PaginateQuery, userInfos: IUserInfoJWT */ ) {
    // const user = await this.userService.findOne(userInfos.id);
    return `This action returns all contract`;
  }

  findOne(id: number) {
    return this.contractRepository.findOneBy({id}); ;
  }

  async update(id: number, updateContractDto: UpdateContractDto) {
    await this.contractRepository.update(id, updateContractDto);
    console.log(`This action updates a #${id} contract`) ;
    return this.contractRepository.findOneBy({id});
  }

  async archived(contract: Contract) {
   await this.contractRepository
   .createQueryBuilder()
   .update(contract)
   .set({archived: true})
   .where("id = :id", { id: contract.id })
   .execute();
    return JSON.stringify('contract archived');
  }

  async unarchived(contract: Contract) {
    await this.contractRepository
    .createQueryBuilder()
    .update(contract)
    .set({archived: false})
    .where("id = :id", { id: contract.id })
    .execute();
     return JSON.stringify('contract unarchived');
   }

  async remove(id: number) {
    console.log(
      `This action removes a #${id} contract`
    )
    return await this.contractRepository.delete(id); ;
  }
}
