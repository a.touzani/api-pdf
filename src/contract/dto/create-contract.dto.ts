import { ApiProperty } from "@nestjs/swagger";

export class CreateContractDto {
    @ApiProperty({description: 'identity'})
    identityCapture: string;

    @ApiProperty({description: 'rib'})
    ribCapture: string;

    @ApiProperty({description: 'cgvsignature'})
    CGVSignature: string;

    @ApiProperty({description: 'old_contract'})
    oldContract: string;
}
