const STATUS_QUOTE = 0;
const STATUS_CANCEL = 10;
const STATUS_SIGNED = 100;
const STATUS_INVALID = 200;
const STATUS_VALID = 300;
const STATUS_WAIT = 400;
const STATUS_DUPLICATE = 500;
const STATUS_VERIF = 600;
const STATUS_RESIGN = 700;
const STATUS_CANCEL_REPLACE = 800;
const STATUS_OLD_CONTRACT = 900;

export const STATUS = [
  { value: STATUS_QUOTE, label: 'Devis', name: 'Devis', color: 'primary' },
  { value: STATUS_CANCEL, label: 'Annulé', name: 'Annulé', color: 'danger' },
  { value: STATUS_SIGNED, label: 'Signé', name: 'Signé', color: 'secondary' },
  {
    value: STATUS_INVALID,
    label: 'Invalide',
    name: 'Invalide',
    color: 'warning',
  },
  { value: STATUS_VALID, label: 'Validé', name: 'Validé', color: 'success' },
  {
    value: STATUS_WAIT,
    label: 'En attente',
    name: 'En attente',
    color: 'warning',
  },
  {
    value: STATUS_DUPLICATE,
    label: 'Doublon',
    name: 'Doublon',
    color: 'danger',
  },
  {
    value: STATUS_VERIF,
    label: 'A vérifier',
    name: 'A vérifier',
    color: 'info',
  },
  {
    value: STATUS_RESIGN,
    label: 'A re-signer',
    name: 'A re-signer',
    color: 'info',
  },
  {
    value: STATUS_CANCEL_REPLACE,
    label: 'Annule et remplace',
    name: 'Annule et remplace',
    color: 'danger',
  },
  {
    value: STATUS_OLD_CONTRACT,
    label: 'Ancien contrat',
    name: 'Ancien contrat',
    color: 'danger',
  },
];

const STATUS_RECONDUCTION_START = 0;
const STATUS_RECONDUCTION_AWAIT = 100;
const STATUS_RECONDUCTION_DONE = 200;
const STATUS_RECONDUCTION_CANCEL = 300;

export const STATUS_RECONDUCTION = [
  {
    value: STATUS_RECONDUCTION_START,
    label: 'Start',
    description: 'Contract créé',
  },
  {
    value: STATUS_RECONDUCTION_AWAIT,
    label: 'Await',
    description: 'En attente de reconduction',
  },
  {
    value: STATUS_RECONDUCTION_DONE,
    label: 'Done',
    description: 'Contrat reconduit',
  },
  {
    value: STATUS_RECONDUCTION_CANCEL,
    label: 'Cancel',
    description: 'Contract non reconduit',
  },
];
