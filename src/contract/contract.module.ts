import { Module } from '@nestjs/common';
import { ContractService } from './contract.service';
import { ContractController } from './contract.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Contract } from './entities/contract.entity';
import { ContractInformation } from 'src/contract-information/entities/contract-information.entity';
import { Customer } from 'src/customer/entities/customer.entity';
import { LevelService } from 'src/level-service/entities/level-service.entity';
import { User } from 'src/user/entities/user.entity';
import { Document } from 'src/document/entities/document.entity';

@Module({
  imports: [TypeOrmModule.forFeature([
    Contract, 
    ContractInformation,
    Customer,
    ContractInformation,
    LevelService,
    User,
    Document,
  ])],
  controllers: [ContractController],
  providers: [ContractService],
})
export class ContractModule {}