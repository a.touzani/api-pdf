import {
    Column,
    CreateDateColumn,
    Entity,
    JoinColumn,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
  } from 'typeorm';
  import { Customer } from '../../customer/entities/customer.entity';
  import { Appointment } from '../../appointment/entities/appointment.entity';
  import { Team } from '../../team/entities/team.entity';
  import { Sector } from '../../sector/entities/sector.entity';
  import { ContractInformation } from '../../contract-information/entities/contract-information.entity';
  import { LevelService } from '../../level-service/entities/level-service.entity';
  import { User } from '../../user/entities/user.entity';
  import { Document } from '../../document/entities/document.entity';
import { STATUS } from '../dto/constants';
  
  @Entity('contract')
  export class Contract {
    @PrimaryGeneratedColumn()
    id: number;
  
    @OneToMany(
      () => ContractInformation,
      (contractInformation) => contractInformation.contract,
      {
        onDelete: 'CASCADE',
        orphanedRowAction: 'delete',
      },
    )
    contractInformation: ContractInformation[];
  
    @ManyToOne(() => LevelService, (levelService) => levelService.id)
    @JoinColumn({ name: 'level_service' })
    levelService: LevelService;
  
    @Column()
    identityCapture: string;
  
    @Column()
    ribCapture: string;
  
    @Column({ type: 'text', nullable: true })
    internNote: string;
  
    @Column({ type: 'text', nullable: true })
    note: string;
  
    @Column({ nullable: true })
    iban: string;
  
    @Column({ nullable: true })
    bic: string;
  
    @Column({ nullable: true })
    reference: string;
  
    @Column({ name: 'contract_signature' })
    contractSignature: string;
  
    @Column({ name: 'old_contract' })
    oldContract: string;
  
    @Column({ name: 'sepa_signature' })
    sepaSignature: string;
  
    @Column({ name: 'contract_seller_signature' })
    contractSellerSignature: string;
  
    @ManyToOne(() => User, (user) => user.id)
    @JoinColumn({ name: 'seller' })
    seller: User;
  
    @Column({ name: 'cgvsignature' })
    CGVSignature: string;
  
    @Column({ nullable: true })
    reconduction: number;
  
    @Column({ name: 'signature_date', type: 'datetime', nullable: true })
    signatureDate: Date;
  
    @Column({ name: 'cancelation_date', type: 'datetime', nullable: true })
    cancelationDate: Date;
  
    @ManyToOne(() => Customer, (customer) => customer.contracts)
    @JoinColumn({ name: 'customer' })
    customer: Customer;
  
    @OneToMany(() => Appointment, (appointment) => appointment.contract)
    appointments: Appointment[];
  
    @CreateDateColumn({ name: 'created', type: 'datetime' })
    created: Date;
  
    @UpdateDateColumn({ name: 'updated', type: 'datetime' })
    updated: Date;
  
    @Column({ default: 0 })
    status: number;
  
  
    @Column({ name: 'installation_date', type: 'datetime', nullable: true })
    installationDate: Date;
  
    @Column({ name: 'entretien_date', type: 'datetime', nullable: true })
    entretienDate: Date;
  
    @Column({ nullable: true })
    promotion: number;
  
    @Column({ nullable: true, type: 'decimal' })
    monthly: number;
  
    @Column({ name: 'payment_type', nullable: true })
    paymentType: string;
  
    @Column({ nullable: true })
    fitter: string;
  
    @Column({ nullable: true })
    identity: string;
    
    
    @ManyToOne(() => Document, (document) => document.contract)
    @JoinColumn({ name: 'document' })
    document: Document
  
    @ManyToOne(() => Document, (document) => document.contract)
    @JoinColumn({ name: 'sepa' })
    sepa: Document;
  
    @ManyToOne(() => Document, (document) => document.id, {
      cascade: true,
    })
    @JoinColumn({ name: 'replace_document_id' })
    replaceDocument: Document;
  
    @ManyToOne(() => Document, (document) => document.id, {
      cascade: true,
    })
    @JoinColumn({ name: 'attestation_tva_id' })
    attestationTVA: Document;
  
    @Column({ nullable: true })
    place: string;
  
    @Column({ nullable: true, default: true })
    synchronised: boolean;
  
    @Column({ name: 'pre_contrat', nullable: true, default: false })
    preContrat: boolean;
  
    @Column({ nullable: true, default: false })
    valid: boolean;
  
    @Column({ name: 'already_installed', nullable: true })
    alreadyInstalled: boolean;
  
    @Column({ nullable: true })
    parc: boolean;
  
    @Column({ name: 'first_reglement', nullable: true })
    firstReglement: boolean;
  
    @Column({ name: 'thermostat_offer', nullable: true })
    thermostatOffer: boolean;
    
    @Column({ nullable: true, default: false })
    archived: boolean;
  
    @ManyToOne(() => User, (user) => user.id)
    @JoinColumn({ name: 'technician_id' })
    technician: User;
  
    @ManyToOne(() => Team, (team) => team.contractTechnique)
    @JoinColumn({ name: 'team_technique_id' })
    teamTechnique: Team;
  
    @ManyToOne(() => Sector, (sector) => sector.contracts, {
      onDelete: 'SET NULL',
    })
    @JoinColumn({ name: 'sector_id' })
    sector: Sector;
  
    statusName: string;
    statusColor: string;
      static document: any;
      static STATUS_QUOTE = 0;
      static STATUS_CANCEL = 10;
      static STATUS_SIGNED = 100;
      static STATUS_INVALID = 200;
      static STATUS_VALID = 300;
      static STATUS_WAIT = 400;
      static STATUS_DUPLICATE = 500;
      static STATUS_VERIF = 600;
      static STATUS_RESIGN = 700;
      static STATUS_CANCEL_REPLACE = 800;
      static STATUS_OLD_CONTRACT = 900;
  
    get getStatusName(): string {
      const index = STATUS.findIndex((v) => v.value === this.status);
      return index !== -1 ? STATUS[index].name : '';
    }
  
    get getStatusColor(): string {
      const index = STATUS.findIndex((v) => v.value === this.status);
      return index !== -1 ? STATUS[index].color : '';
    }
  
  
  
  }
  