import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { ContractService } from './contract.service';
import { CreateContractDto } from './dto/create-contract.dto';
import { UpdateContractDto } from './dto/update-contract.dto';
import { DeleteResult } from 'typeorm';

@Controller('contract')
export class ContractController {
  constructor(private readonly contractService: ContractService) {}

  @Post()
  create(@Body() createContractDto: CreateContractDto) {
    return this.contractService.create(createContractDto);
  }

  @Get()
  findAll() {
    return this.contractService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.contractService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string, 
    @Body() updateContractDto: UpdateContractDto) {
    return this.contractService.update(+id, updateContractDto);
  }

  @Delete(':id')
  async remove(@Param('id') id: number) {
    return await this.contractService.remove(+id);
  }

  @Post('/archived')
  async archived(@Body() id: number){
  const contract = await this.contractService.findOne(id);
  return this.contractService.archived(contract)
  }
  @Post('/unarchived')
  async unarchived(@Body() id: number){
  const contract = await this.contractService.findOne(id);
  return this.contractService.unarchived(contract)
  }
  
}
