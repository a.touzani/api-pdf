import {
    Column,
    Entity,
    JoinColumn,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
  } from 'typeorm';
  import { Appointment } from '../../appointment/entities/appointment.entity';
  import { Team } from '../../team/entities/team.entity';
  
  @Entity()
  export class User {
    @PrimaryGeneratedColumn()
    id: number;
  
    @Column({ nullable: true })
    firstname: string;
  
    @Column({ nullable: true })
    lastname: string;
  
    @Column()
    email: string;
  
    @Column({ name: 'password' })
    password: string;
  
    @Column({ name: 'email_canonical' })
    email_canonical: string;
  
    @Column()
    username: string;
  
    @Column({ name: 'username_canonical' })
    username_canonical: string;
  
    @ManyToOne(() => Team, (team) => team.users)
    @JoinColumn({ name: 'team_id' })
    team: Team;
  
    @OneToMany(() => Team, (team) => team.userMaster)
    teamsMaster: Team[];
  
    @Column()
    enabled: boolean;
  
    @Column()
    salt: string;
  
    @Column({ name: 'last_login', type: 'datetime', nullable: true })
    last_login: Date;
  
    @Column({ name: 'confirmation_token' })
    confirmation_token: string;
  
    @Column({ name: 'password_requested_at', type: 'datetime', nullable: true })
    password_requested_at: Date;
  
    @Column({ type: 'text' })
    roles: string;
  
    @OneToMany(() => Appointment, (appointment) => appointment.technician)
    technicianAppointment: Appointment[];
  }