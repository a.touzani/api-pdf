import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { User } from './entities/user.entity';
import { Team } from 'src/team/entities/team.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Appointment } from 'src/appointment/entities/appointment.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      User,
      Team,
      Appointment,
    ]),
  ],
  controllers: [UserController],
  providers: [UserService],
})
export class UserModule {}
