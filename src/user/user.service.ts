import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { Repository } from 'typeorm';
import { PaginateConfig, PaginateQuery, paginate } from 'nestjs-paginate';

@Injectable()
export class UserService {

  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>
  ) {}

  create(createUserDto: CreateUserDto) {
    const user = this.userRepository.create(createUserDto);
    console.log('User created', user);
    return this.userRepository.save(user);
  }

  async findAll(filtres: PaginateQuery) {
    const query = this.userRepository
      .createQueryBuilder('user')
      .select('user.id')
      .addSelect('user.firstname')
      .addSelect('user.lastname')
      .addSelect('user.email')
      .addSelect('user.username')
      .addSelect('user.roles')
    
    if (filtres.filter?.username){
      query.andWhere('user.username = LIKE :username', {
        username: `%${filtres.filter.username}%`
      });
    }
    if (filtres.filter?.email){
      query.andWhere('user.email = LIKE :email', {
        email: `%${filtres.filter.email}%`
      })
    }
    if (filtres.filter?.enabled) {
      query.andWhere('user.enabled = :enabled', {
        enabled: `%${filtres.filter.enabled}%`
      })
    }query.take(filtres.limit);
    query.skip(filtres.limit * (filtres.page ?? 1 - 1));
    const config: PaginateConfig<User> = {
      defaultSortBy: [['id', 'ASC']],
      filterableColumns: undefined,
      searchableColumns: ['firstname', 'lastname', 'email', 'username'],
      select: [
        'id',
        'firstname',
        'lastname',
        'email',
        'username',
        'enabled',
        'roles',
      ],
      sortableColumns: ['id'],
    };
    return await paginate<User>(filtres, query, config);
  }

  findOne(id: number) {
    return this.userRepository.findOne({
      select: [
        'id', 
        'firstname',
        'lastname',
        'email',
        'username',
        'enabled',
        'roles'
      ],
      where: {id: id}
    })
  }

  update(id: number, updateUserDto: UpdateUserDto) {
    return this.userRepository.update(id, updateUserDto);
  }

  remove(id: number) {
    return this.userRepository.delete(id);
  }
}
