// import { Module } from '@nestjs/common';
// import * as fs from 'fs';
// import { PassportModule } from '@nestjs/passport';
// import { JwtModule } from '@nestjs/jwt';
// import { ConfigModule, ConfigService } from '@nestjs/config';
// import { JwtStrategy } from './jwt.strategy';
// import { HeaderApiKeyStrategy } from './auth-header-api-key-strategy';

// @Module({
//   imports: [
//     PassportModule,
//     JwtModule.registerAsync({
//       imports: [ConfigModule],
//       inject: [ConfigService],
//       useFactory: async (configService: ConfigService) => {
//         const publicKey = fs
//           .readFileSync(
//             `${process.cwd()}/dist/${configService.get<string>(
//               'authentication.jwtOptions.publicKeyPath',
//             )}`,
//           )
//           .toString();

//         const privateKey = {
//           key: fs
//             .readFileSync(
//               `${process.cwd()}/dist/${configService.get<string>(
//                 'authentication.jwtOptions.privateKeyPath',
//               )}`,
//             )
//             .toString(),
//           passphrase: process.env.JWT_SECRET,
//         };
//         return {
//           publicKey,
//           privateKey,
//           signOptions: {
//             expiresIn: configService.get<string>(
//               'authentication.jwtOptions.expiresIn',
//             ),
//             algorithm: 'RS256',
//           },
//         };
//       },
//     }),
//   ],
//   providers: [JwtStrategy, HeaderApiKeyStrategy],
//   controllers: [],
//   exports: [],
// })
// export class AuthModule {}
