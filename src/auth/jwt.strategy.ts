// import { ExtractJwt, Strategy } from 'passport-jwt';
// import { PassportStrategy } from '@nestjs/passport';
// import { Injectable } from '@nestjs/common';
// import { ConfigService } from '@nestjs/config';
// import * as fs from 'fs';

// @Injectable()
// export class JwtStrategy extends PassportStrategy(Strategy) {
//   constructor(configService: ConfigService) {
//     const secretOrKey = fs
//       .readFileSync(
//         `${process.cwd()}/dist/${configService.get<string>(
//           'authentication.jwtOptions.publicKeyPath',
//         )}`,
//       )
//       .toString();

//     super({
//       jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
//       ignoreExpiration: false,
//       secretOrKey,
//     });
//   }

//   async validate(payload: any) {
//     return payload;
//   }
// }
