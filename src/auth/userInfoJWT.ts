interface IUserInfoJWT {
    userId?: number;
    userName?: string;
    email?: string;
    roles?: string;
    // jwt?: string; 
}