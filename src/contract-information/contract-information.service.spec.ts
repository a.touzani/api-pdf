import { Test, TestingModule } from '@nestjs/testing';
import { ContractInformationService } from './contract-information.service';

describe('ContractInformationService', () => {
  let service: ContractInformationService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ContractInformationService],
    }).compile();

    service = module.get<ContractInformationService>(ContractInformationService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
