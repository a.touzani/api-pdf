import { PartialType } from '@nestjs/mapped-types';
import { CreateContractInformationDto } from './create-contract-information.dto';

export class UpdateContractInformationDto extends PartialType(CreateContractInformationDto) {}
