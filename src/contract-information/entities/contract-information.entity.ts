import {
    Column,
    Entity,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
  } from 'typeorm';
  import { Contract } from '../../contract/entities/contract.entity';
  import { EquipmentFamily } from '../../equipment-family/entities/equipment-family.entity';
  import { Equipment } from '../../equipment/entities/equipment.entity';
  
  @Entity()
  export class ContractInformation {
    @PrimaryGeneratedColumn()
    id: number;
  
    @ManyToOne(() => Contract, (contract) => contract.contractInformation)
    @JoinColumn({ name: 'contract' })
    contract: Contract;
  
    @ManyToOne(
      () => EquipmentFamily,
      (equipmentFamily) => equipmentFamily.equipments,
    )
    @JoinColumn({ name: 'equipment-family' })
    equipmentFamily: EquipmentFamily;
  
    //many to one with equipment and name is type
    @ManyToOne(() => Equipment, (equipment) => equipment.id)
    @JoinColumn({ name: 'type' })
    type: Equipment;
  
    @Column({ name: 'kit', nullable: true })
    kit: string;
  
    @Column({ name: 'brand', nullable: true })
    brand: string;
  
    @Column({ name: 'power', nullable: true })
    power: string;
  
    @Column({ name: 'year', nullable: true })
    year: string;
  
    @Column({ name: 'qty', nullable: true, default: 1 })
    qty: string;
  
    @Column({ name: 'date', nullable: true })
    date: string;
  
    @Column({ name: 'note', nullable: true })
    note: string;
  
    @Column({ name: 'serial_number', nullable: true })
    serialNumber: string;
  
    @Column({ name: 'nbr_unite', nullable: true })
    nbrUnite: number;
  
    @Column({ name: 'nbr_unite_int', nullable: true })
    nbrUniteInt: number;
  }
  