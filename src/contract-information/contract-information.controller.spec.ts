import { Test, TestingModule } from '@nestjs/testing';
import { ContractInformationController } from './contract-information.controller';
import { ContractInformationService } from './contract-information.service';

describe('ContractInformationController', () => {
  let controller: ContractInformationController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ContractInformationController],
      providers: [ContractInformationService],
    }).compile();

    controller = module.get<ContractInformationController>(ContractInformationController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
