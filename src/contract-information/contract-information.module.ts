import { Module } from '@nestjs/common';
import { ContractInformationService } from './contract-information.service';
import { ContractInformationController } from './contract-information.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ContractInformation } from './entities/contract-information.entity';
import { Equipment } from 'src/equipment/entities/equipment.entity';

@Module({
  imports: [TypeOrmModule.forFeature([
    ContractInformation,
    Equipment,

  ])],
  controllers: [ContractInformationController],
  providers: [ContractInformationService],
})
export class ContractInformationModule {}
