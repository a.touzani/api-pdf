import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { ContractInformationService } from './contract-information.service';
import { CreateContractInformationDto } from './dto/create-contract-information.dto';
import { UpdateContractInformationDto } from './dto/update-contract-information.dto';

@Controller('contract-information')
export class ContractInformationController {
  constructor(private readonly contractInformationService: ContractInformationService) {}

  @Post()
  create(@Body() createContractInformationDto: CreateContractInformationDto) {
    return this.contractInformationService.create(createContractInformationDto);
  }

  @Get()
  findAll() {
    return this.contractInformationService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.contractInformationService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateContractInformationDto: UpdateContractInformationDto) {
    return this.contractInformationService.update(+id, updateContractInformationDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.contractInformationService.remove(+id);
  }
}
