import { Injectable } from '@nestjs/common';
import { CreateContractInformationDto } from './dto/create-contract-information.dto';
import { UpdateContractInformationDto } from './dto/update-contract-information.dto';

@Injectable()
export class ContractInformationService {
  create(createContractInformationDto: CreateContractInformationDto) {
    return 'This action adds a new contractInformation';
  }

  findAll() {
    return `This action returns all contractInformation`;
  }

  findOne(id: number) {
    return `This action returns a #${id} contractInformation`;
  }

  update(id: number, updateContractInformationDto: UpdateContractInformationDto) {
    return `This action updates a #${id} contractInformation`;
  }

  remove(id: number) {
    return `This action removes a #${id} contractInformation`;
  }
}
