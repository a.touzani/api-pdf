import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { EquipmentFamilyService } from './equipment-family.service';
import { CreateEquipmentFamilyDto } from './dto/create-equipment-family.dto';
import { UpdateEquipmentFamilyDto } from './dto/update-equipment-family.dto';

@Controller('equipment-family')
export class EquipmentFamilyController {
  constructor(private readonly equipmentFamilyService: EquipmentFamilyService) {}

  @Post()
  create(@Body() createEquipmentFamilyDto: CreateEquipmentFamilyDto) {
    return this.equipmentFamilyService.create(createEquipmentFamilyDto);
  }

  @Get()
  findAll() {
    return this.equipmentFamilyService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.equipmentFamilyService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateEquipmentFamilyDto: UpdateEquipmentFamilyDto) {
    return this.equipmentFamilyService.update(+id, updateEquipmentFamilyDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.equipmentFamilyService.remove(+id);
  }
}
