import { Module } from '@nestjs/common';
import { EquipmentFamilyService } from './equipment-family.service';
import { EquipmentFamilyController } from './equipment-family.controller';

@Module({
  controllers: [EquipmentFamilyController],
  providers: [EquipmentFamilyService],
})
export class EquipmentFamilyModule {}
