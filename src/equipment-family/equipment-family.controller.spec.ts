import { Test, TestingModule } from '@nestjs/testing';
import { EquipmentFamilyController } from './equipment-family.controller';
import { EquipmentFamilyService } from './equipment-family.service';

describe('EquipmentFamilyController', () => {
  let controller: EquipmentFamilyController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [EquipmentFamilyController],
      providers: [EquipmentFamilyService],
    }).compile();

    controller = module.get<EquipmentFamilyController>(EquipmentFamilyController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
