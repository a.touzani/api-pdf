import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Equipment } from '../../equipment/entities/equipment.entity';

@Entity()
export class EquipmentFamily {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  enabled: boolean;

  @OneToMany(() => Equipment, (equipment) => equipment.family)
  equipments: Equipment[];
}
