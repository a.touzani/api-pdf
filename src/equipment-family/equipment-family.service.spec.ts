import { Test, TestingModule } from '@nestjs/testing';
import { EquipmentFamilyService } from './equipment-family.service';

describe('EquipmentFamilyService', () => {
  let service: EquipmentFamilyService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [EquipmentFamilyService],
    }).compile();

    service = module.get<EquipmentFamilyService>(EquipmentFamilyService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
