import { Injectable } from '@nestjs/common';
import { CreateEquipmentFamilyDto } from './dto/create-equipment-family.dto';
import { UpdateEquipmentFamilyDto } from './dto/update-equipment-family.dto';

@Injectable()
export class EquipmentFamilyService {
  create(createEquipmentFamilyDto: CreateEquipmentFamilyDto) {
    return 'This action adds a new equipmentFamily';
  }

  findAll() {
    return `This action returns all equipmentFamily`;
  }

  findOne(id: number) {
    return `This action returns a #${id} equipmentFamily`;
  }

  update(id: number, updateEquipmentFamilyDto: UpdateEquipmentFamilyDto) {
    return `This action updates a #${id} equipmentFamily`;
  }

  remove(id: number) {
    return `This action removes a #${id} equipmentFamily`;
  }
}
