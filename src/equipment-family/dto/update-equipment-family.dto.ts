import { PartialType } from '@nestjs/mapped-types';
import { CreateEquipmentFamilyDto } from './create-equipment-family.dto';

export class UpdateEquipmentFamilyDto extends PartialType(CreateEquipmentFamilyDto) {}
