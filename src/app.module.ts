import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ContractModule } from './contract/contract.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Contract } from './contract/entities/contract.entity';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule } from '@nestjs/config';
import { CustomerModule } from './customer/customer.module';
import { AppointmentModule } from './appointment/appointment.module';
import { UserModule } from './user/user.module';
import { TeamModule } from './team/team.module';
import { SectorModule } from './sector/sector.module';
import { ContractInformationModule } from './contract-information/contract-information.module';
import { EquipmentModule } from './equipment/equipment.module';
import { EquipmentFamilyModule } from './equipment-family/equipment-family.module';
import { LevelServiceModule } from './level-service/level-service.module';
import { DocumentModule } from './document/document.module';
import configuration from 'config/configuration';

console.log( '.env 🎾' ,process.env.DATABASE_HOST)

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [configuration]
    }),
  
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: process.env.DATABASE_HOST,
      port: +process.env.DATABASE_PORT,
      username: process.env.DATABASE_USER,
      password: process.env.DATABASE_PASSWORD,
      database: process.env.DATABASE_NAME,
      entities: [
        Contract,
      ],
      autoLoadEntities: true,
      synchronize: false,
      // logging: true
      
    }),
    ContractModule,
    CustomerModule,
    AppointmentModule,
    UserModule,
    TeamModule,
    SectorModule,
    ContractInformationModule,
    EquipmentModule,
    EquipmentFamilyModule,
    LevelServiceModule,
    DocumentModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})

export class AppModule {}
