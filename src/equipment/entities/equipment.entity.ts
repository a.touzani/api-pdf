import {
    Column,
    Entity,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
  } from 'typeorm';
  import { EquipmentFamily } from '../../equipment-family/entities/equipment-family.entity';
  
  @Entity()
  export class Equipment {
    @PrimaryGeneratedColumn()
    id: number;
  
    @Column()
    name: string;
  
    @ManyToOne(
      () => EquipmentFamily,
      (equipmentFamily) => equipmentFamily.equipments,
    )
    @JoinColumn({ name: 'family' })
    family: EquipmentFamily;
  
    @Column({ nullable: true })
    enabled: boolean;
  }
  