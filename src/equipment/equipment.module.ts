import { Module } from '@nestjs/common';
import { EquipmentService } from './equipment.service';
import { EquipmentController } from './equipment.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Equipment } from './entities/equipment.entity';
import { EquipmentFamily } from 'src/equipment-family/entities/equipment-family.entity';

@Module({
  imports: [TypeOrmModule.forFeature([
    Equipment,
    EquipmentFamily,
  ])],
  controllers: [EquipmentController],
  providers: [EquipmentService],
})
export class EquipmentModule {}
