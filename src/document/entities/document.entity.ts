import { Contract } from 'src/contract/entities/contract.entity';
import { Column, Entity, ManyToMany, PrimaryGeneratedColumn, JoinColumn, OneToMany } from 'typeorm';

@Entity('document')
export class Document {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'file_path' })
  filePath: string;

  @Column({ default: 0 })
  status: number;

  @OneToMany(() => Contract, (contract) => contract.document)
  @JoinColumn({ name: 'contract_id' })
  contract: Contract[]
}

