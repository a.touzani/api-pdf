import {
    Column,
    Entity,
    JoinColumn,
    OneToMany,
    OneToOne,
    PrimaryGeneratedColumn,
  } from 'typeorm';
  import { User } from '../../user/entities/user.entity';
  import { Team } from '../../team/entities/team.entity';
  import { Contract } from '../../contract/entities/contract.entity';
  
  @Entity()
  export class Sector {
    @PrimaryGeneratedColumn()
    id: number;
  
    @Column()
    name: string;
  
    @OneToMany(() => Team, (team) => team.sector)
    teams: Team[];
  
    @OneToOne(() => User)
    @JoinColumn({ name: 'user_master_id' })
    UserMaster: User;
  
    @Column({ name: 'sector_type', nullable: true })
    sectorType: string;
  
    @OneToMany(() => Contract, (contract) => contract.sector)
    contracts: Contract[];
  }
  