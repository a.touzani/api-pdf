import { Module } from '@nestjs/common';
import { SectorService } from './sector.service';
import { SectorController } from './sector.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Team } from 'src/team/entities/team.entity';
import { Sector } from './entities/sector.entity';

@Module({
  imports: [TypeOrmModule.forFeature([
    Team,
    Sector,

  ])],
  controllers: [SectorController],
  providers: [SectorService],
})
export class SectorModule {}
