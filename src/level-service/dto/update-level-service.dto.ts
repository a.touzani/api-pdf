import { PartialType } from '@nestjs/mapped-types';
import { CreateLevelServiceDto } from './create-level-service.dto';

export class UpdateLevelServiceDto extends PartialType(CreateLevelServiceDto) {}
