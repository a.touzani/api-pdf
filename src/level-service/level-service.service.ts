import { Injectable } from '@nestjs/common';
import { CreateLevelServiceDto } from './dto/create-level-service.dto';
import { UpdateLevelServiceDto } from './dto/update-level-service.dto';

@Injectable()
export class LevelServiceService {
  create(createLevelServiceDto: CreateLevelServiceDto) {
    return 'This action adds a new levelService';
  }

  findAll() {
    return `This action returns all levelService`;
  }

  findOne(id: number) {
    return `This action returns a #${id} levelService`;
  }

  update(id: number, updateLevelServiceDto: UpdateLevelServiceDto) {
    return `This action updates a #${id} levelService`;
  }

  remove(id: number) {
    return `This action removes a #${id} levelService`;
  }
}
