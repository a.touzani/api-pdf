import { Module } from '@nestjs/common';
import { LevelServiceService } from './level-service.service';
import { LevelServiceController } from './level-service.controller';

@Module({
  controllers: [LevelServiceController],
  providers: [LevelServiceService],
})
export class LevelServiceModule {}
