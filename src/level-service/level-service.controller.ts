import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { LevelServiceService } from './level-service.service';
import { CreateLevelServiceDto } from './dto/create-level-service.dto';
import { UpdateLevelServiceDto } from './dto/update-level-service.dto';

@Controller('level-service')
export class LevelServiceController {
  constructor(private readonly levelServiceService: LevelServiceService) {}

  @Post()
  create(@Body() createLevelServiceDto: CreateLevelServiceDto) {
    return this.levelServiceService.create(createLevelServiceDto);
  }

  @Get()
  findAll() {
    return this.levelServiceService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.levelServiceService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateLevelServiceDto: UpdateLevelServiceDto) {
    return this.levelServiceService.update(+id, updateLevelServiceDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.levelServiceService.remove(+id);
  }
}
