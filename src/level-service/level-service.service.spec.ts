import { Test, TestingModule } from '@nestjs/testing';
import { LevelServiceService } from './level-service.service';

describe('LevelServiceService', () => {
  let service: LevelServiceService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [LevelServiceService],
    }).compile();

    service = module.get<LevelServiceService>(LevelServiceService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
