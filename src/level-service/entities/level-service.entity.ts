import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class LevelService {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;
}
