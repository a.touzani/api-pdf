import { Test, TestingModule } from '@nestjs/testing';
import { LevelServiceController } from './level-service.controller';
import { LevelServiceService } from './level-service.service';

describe('LevelServiceController', () => {
  let controller: LevelServiceController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [LevelServiceController],
      providers: [LevelServiceService],
    }).compile();

    controller = module.get<LevelServiceController>(LevelServiceController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
